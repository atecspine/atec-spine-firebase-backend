document.addEventListener('DOMContentLoaded', function() {
  let app = firebase.app();
  let params = new URLSearchParams(window.location.search);
  let apiKey = params.get('apiKey');
  if (apiKey !== app.options.apiKey) {
    window.location = '/';
  }
  let token = params.get('token');
  firebase.auth().signInWithCustomToken(token).catch((error) => {
    // Handle Errors here.
    alert(error.message);
    window.location = '/';
  });

  $('#changePasswordForm').submit(() => {
    let password = $('#inputPassword').val();
    let confirmPassword = $('#inputConfirmPassword').val();
    if (password !== confirmPassword) {
      alert('Passwords don\'t match!');
    } else {
      var user = firebase.auth().currentUser;

      user.updatePassword(password).then(() => {
        // Update successful.
        $('.card-body').text('Your password has been set! You can now log into the app with it.');
      }).catch(function(error) {
        // An error happened.
        alert(error.message);
      });
    }
    return false;
  });
});