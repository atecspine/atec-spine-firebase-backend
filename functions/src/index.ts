import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
const serviceAccount = require("../service-account-credentials.json");
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://atec-spine-dev.firebaseio.com"
});
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(functions.config().sendgrid.key);
sgMail.setSubstitutionWrappers('{{', '}}');


export const sendWelcomeEmail = functions.auth.user().onCreate(user => {
  // Create a one-time use token and save it for user
  return admin.auth().createCustomToken(user.uid).then(token => {
    // Send custom email with token to change password page
    const url = `https://atec-spine-dev.firebaseapp.com/users/updatePassword?token=${token}&apiKey=AIzaSyB1IPo0-QKii4EQYVRtCmWiPKH53mYBEs8`;
    const msg = {
      to: user.email,
      from: 'noreply@atecspine.com',
      subject: 'Welcome to the ATECspine App',
      substitutions: {
        link: url,
      },
      templateId: functions.config().sendgrid.template_id,
    };
    return sgMail.send(msg);
  });
});

export const updateNewsletters = functions.firestore.document('newsletters/{docId}').onCreate(snapshot => {
  const doc = snapshot.ref; 

  // Set the newest newsletter to show up first in list
  return doc.update({
    order: 0,
  }).then(() => {
    // Update the rest of the newsletters' order
    return admin.firestore().collection('newsletters').get().then((querySnapshot) => {
      querySnapshot.forEach(snap => {
        const ref = snap.ref;
        const newsletter = snap.data();
        return ref.update({
          order: newsletter.order + 1
        });
      });
    });
  }).catch(err => console.log('Error: ', err));
})
